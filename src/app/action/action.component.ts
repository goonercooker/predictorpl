import { Component, OnInit } from '@angular/core';
import { Papa } from 'ngx-papaparse';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

  res = [];
  results = {};
  predictions = [];
  showBtn = false;
  showTable = false;
  constructor(private papa: Papa) { }

  ngOnInit() {
  }

  parse(files: FileList): void {
    const file: File = files.item(0);
    this.papa.parse(file, {
      header: true,
      complete: (result) => {
        console.log('Parsed: ', result);
        this.res = result.data.filter(item => item.Name.length);
        this.results = this.res[0];
        this.predictions = this.res.filter(item => item.Name !== 'Results');
        this.showBtn = true;
        console.log(this.results);
        console.log(this.predictions);
      }
    });
  }

  calculate() {
    console.log('Calculating...');
    this.predictions.forEach((item) => {
      let score = 0;
      const arr = Object.keys(this.results);
      for (let i=1; i<arr.length; i=i+2) {
        const key1 = arr[i];
        const key2 = arr[i+1];
        if ((item[key1] === this.results[key1]) && (item[key2] === this.results[key2])) {
          score += 5;
          continue;
        }
        if (((item[key1] === item[key2]) && (this.results[key1] === this.results[key2])) || ((Number(item[key1]) - Number(item[key2]))*(Number(this.results[key1]) - Number(this.results[key2])) > 0)) score += 2;
        if (((Number(item[key1]) - Number(item[key2])) === (Number(this.results[key1]) - Number(this.results[key2]))) && ((Number(item[key1]) - Number(item[key2]))*(Number(this.results[key1]) - Number(this.results[key2])) > 0)) score++;
        if ((item[key1] === this.results[key1]) || (item[key2] === this.results[key2])) score++;
      }
      (item as any).score = score;
      console.log('Name - ' + item.Name + ', Score - ' + item.score);
    });
    this.predictions.sort(function(a, b) {
      return b.score - a.score;
    });
    this.showTable = true;
  }

}
